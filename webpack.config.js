module.exports = {
  devtool: 'sourcemap',
  output: {
    filename: 'bundle.js'
  },
  module: {
    loaders: [
       { test: /\.js$/, exclude: [/app\/lib/, /node_modules/], loader: 'babel' },
       { test: /\.html$/, loader: 'raw' },
       { test: /\.jade$/, loader: 'jade' },
       { test: /\.styl$/, loader: 'style!css!stylus' },
       { test: /\.sass$/, loader: 'style!css!sass' },
       { test: /\.css$/, loader: 'style!css' }
    ]
  }
};
