import angular from 'angular';
import Home from './home/home';
import About from './about/about';

let components = [
    Home.name,
    About.name
];

let componentModule = angular.module('app.components', components)
    .constant('components', components);

export default componentModule;
